/**
 * ICI on instancie nos classes et on pose nos listeners
 */
 //TODO:
//-Maquette
//-Intégration Boostrap
//-

// Déclaration variables globales
var userData = [];
var mail, age, name, currentUser;

// Quand la page a fini de "charger" on initialise nos variables et on lance la fonction loadUserInData()
$(function() {
    mail = $('#email');
    age = $('#age');
    name = $('#user-name');

    init();

    // Quand on click sur le bouton du formulaire #add-user
    $( "#add-user" ).submit(function( event ) {
        //preventDefault() evite le rechargement de la page
        event.preventDefault();

        let user;
        let intAge = parseInt(age.val());

        // Si intAge est plus grand ou égale à 16 && plus petit ou  ééégale à 120 alors:
        // On initialise un nouvelle objet User dans la variable user et on lui passe la valeur de mail (récupéré de l'input) comme paramètre,
        // On lui attribut une nouvelle valeur age grâce à la methode de l'objet setAge() -> voir class User dans js/models/user.js
        // On fait de même pour le nom
        // Enfin on ajoute la variable user au tableau userData (méthode push())
        if (intAge >= 16 && intAge <= 120) {
            user = new User(mail.val());
            user.setAge(intAge);
            user.setUserName($('#user-name').val());
            userData.push(user);
            addToUserList();
        }


        // Pour chaque balise input contenus dans le formulaire #add-user
        // on réinitialise la valeur par rien! Cela permet de supprimer les informations rentrées au préalable
        $('#add-user input').each(function() {
            $(this).val("");
        });

    });

    $("#userList").change( function(){
        currentUser = $('body').find('#userList option:selected').val();
    });
});

function init() {
    // Injection des inputs, bouttons et conteneur liste déroulante pour les posts
    $( "#add-post" ).html('<select id="userList"></select><input type="title" name="title-name" id="title" /><label for="text"></label><textarea id="text" name="text"></textarea><button type="submit">Ajouter</button>');

    // Ajout des objets User dans le tableau userData
    loadUserInData();
}


// Boucle le tableau tempData et créer un nouvelle objet User
// Passe les paramètres contenus dans le tableau (email, nom, age)
// Ajoute l'objet dans le tableau userData
function loadUserInData() {
    let tempData=[
        ["nadia@gmail.com", "Nadia", 20],
        ["ilhem@gmail.com", "Ilhem", 20],
        ["mubarak@gmail.com", "Mubarak", 20],
        ["lionel@gmail.com", "Lionel", 20],
    ]
    let addUser;
    for (var t = 0; t < tempData.length; t++) {
        for (var i = 0; i < tempData[t].length; i++) {
            if (i == 0) {
                addUser = new User(tempData[t][i])
            }else if (i == 1) {
                addUser.setUserName(tempData[t][i])
            }else {
                addUser.setAge(tempData[t][i]);
            }
        }
        userData.push(addUser);
    }
    addToUserList();
}

// Inject des balises options en fonction du nombre de User dans userData
function addToUserList() {
    var sel = $('body').find('#userList');
    let nbOption = $('body').find('#userList option').length;
     

    $(userData).each(function(index) {
        if (nbOption == 0 || index >= nbOption) {
            $('<option>').appendTo(sel).attr('value',this.getUserName()).text(this.getUserName());
        }
    });
}
  